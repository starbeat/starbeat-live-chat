import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from "react-native";
import { LiveStreamComp } from 'starbeat-rn-livestream';
import { HOST, BASE_URL, NORMAL, QUESTIONS, ITEM_SEPERATOR, QUIZ, POLL } from 'starbeat-rn-livestream';
import database from '@react-native-firebase/database';
import { ATTENDEE, PREMIUM } from '../../src/utils/Constants';

class VideoCall extends Component {

    constructor(props) {
        super(props);
        this.state = {
            winnersList: [],
            resultForQues: {}
        }
    }

    onCallEnded() {
        this.props.navigation.goBack();
    }

    render() {
        var userId = this.props.route.params.user == HOST ? 12345 : 98765
        if (this.props.route.params.user == ATTENDEE) {
            userId = 0
        }
        return (
            <View style={Styles.root}>
                {/* <LiveStreamComp
                    callUrl={this.props.route.params.url}
                    user={this.props.route.params.user}
                    userName={this.props.route.params.userName}
                    email={this.props.route.params.email}
                    userImageUrl={this.props.route.params.userImageUrl}
                    onCallEnded={this.onCallEnded.bind(this)}
                    onOptionSelected={this.onOptionSelected.bind(this)} /> */}

                <LiveStreamComp
                    callUrl={this.props.route.params.url}
                    user={this.props.route.params.user}
                    userName={this.props.route.params.userName}
                    email={this.props.route.params.email}
                    userImageUrl={this.props.route.params.userImageUrl}
                    onCallEnded={this.onCallEnded.bind(this)}
                    videoMode={PREMIUM}
                    chatEnable={this.props.route.params.chatEnable}
                    reactionsArr={this.props.route.params.reactionsArr}
                    isPoll={this.props.route.params.isPoll}
                    reAttempts={this.props.route.params.reAttempts}
                    initialTimeLimit={this.props.route.params.initialTimeLimit}
                    authToken={'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzYXJhdmFuYSIsImp0aSI6IkV4dGVybmFsIiwiYXV0aG9yaXRpZXNfa2V5IjoiTk9STUFMIiwidXNlcl9pZCI6MiwiZXhwIjoxNjMxNTc3NzI4fQ.7o1IFLkLBhX8RtLUPyg8V1N_RfkI6I_4elpwRN_mLkoJ13W-HtOoc_uJs85iCOyVFZJtj_YqKRqX8OoYO3ajrQ'}
                    token={'00654378363c5f74b4e997dab4e384a32e5IABLJCRcprufJiIM1agnH5ADMIWRAXgy0m7vTrJPMldZQ20lkvIAAAAAEABuqWSufMHBYQEAAQB7wcFh'}
                    agoraAppId={'54378363c5f74b4e997dab4e384a32e5'}
                    userId={userId}
                    eventId={'1221'}
                    eventType={POLL}
                    userName="starbeat"
                    password="SB3atC0nf_Pass"
                />

            </View>
        )
    }
}

const Styles = StyleSheet.create({
    root: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'tomato'
    },
    text: {
        fontSize: 14,
        color: "#000000",
        margin: 24,
        textAlign: 'center'
    },
})

export default (VideoCall)