import { combineReducers } from '@reduxjs/toolkit';
import { chatsReducer } from 'starbeat-rn-livestream';

export default combineReducers({
    chats: chatsReducer
});