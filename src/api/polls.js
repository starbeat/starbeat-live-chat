import { create } from 'apisauce';

function getApi(authToken) {
    const defaultOptions = {
        baseURL: 'https://dev.starbeat.com',
        headers: {
            'X-AUTH-TOKEN': authToken
        }
    };
    return create(defaultOptions);
}

const GET_ALL_QUESTIONS = "/user/mobile/api/v1/event/"
const RECORD_ANS_FOR_POST = "/user/mobile/api/v1/user/post/"
const GET_RESULT_FOR_QUESTION = "/user/mobile/api/v1/user/post/"
const GET_LEADERBOARD_FOR_EVENT = "/user/mobile/api/v1/event/"

export async function getAllQuestions({ postId, authToken }) {
    const endpoint = GET_ALL_QUESTIONS + postId + '/posts'
    const result = await getApi(authToken).get(endpoint)
    if (result.ok) {
        return {
            data: result.data
        };
    }
}

export async function recordAnsForUser({ postId, postType, authToken, selectdOptionsArray, timeInSec }) {
    const endpoint = RECORD_ANS_FOR_POST + postId + '/' + 'poll'
    const result = await getApi(authToken).post(endpoint, {
        "postPollOptionIds": selectdOptionsArray,
        "timeToRespond": timeInSec
    })
    if (result.ok) {
        return {
            data: result.data
        };
    }
}

export async function getResultForQuestion({ postId, postType, authToken }) {
    const endpoint = GET_RESULT_FOR_QUESTION + postId + '/' + 'poll'
    const result = await getApi(authToken).get(endpoint)
    if (result.ok) {
        return {
            data: result.data
        };
    }
}

export async function getLeaderboardForEvent({ authToken, postId }) {
    const endpoint = GET_LEADERBOARD_FOR_EVENT + postId + '/leaderboard'
    const result = await getApi(authToken).get(endpoint)
    if (result.ok) {
        return {
            data: result.data
        };
    }
}