import { create } from 'apisauce';

function getApi(authToken) {
    const defaultOptions = {
        baseURL: 'https://dev.starbeat.com',
        headers: {
            'X-AUTH-TOKEN': authToken
        }
    };
    return create(defaultOptions);
}

const CREATE_EVENT = "/user/mobile/api/v1/event";

export async function createEvent({data,authToken}) {
  const result = await getApi(authToken).post(CREATE_EVENT, data);
  if (result.ok) {
    return {
      status: true,
      data: result.data.data,
    };
  } else if (result.status == 500) {
    return {
      status: false,
      data: result.data.data,
    };
  }else{
    return{
      status: false,
      data: ''
    }
  }
}