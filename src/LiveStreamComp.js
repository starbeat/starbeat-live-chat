import React from 'react';
import { Alert, StyleSheet, Dimensions, KeyboardAvoidingView, FlatList, Pressable, PermissionsAndroid } from 'react-native';
import { View, BackHandler, TextInput, Image, Text } from 'react-native';
import MessageComp from "./components/MessageComp";
import LiveCounterComp from './components/LiveCounterComp';
import Colors from './utils/Colors'
import Dimens from './utils/Dimens'
import Strings from './utils/Strings'
import { ATTENDEE, HOST, BASE_URL, CO_HOST, NORMAL, PREMIUM, EMOTICONS, UNAUTHORIZED } from './utils/Constants';
import {
    ITEM_SEPERATOR, CHATS, TOTAL_AUDIENCE, CHAT_VISIBILITY, CHATS_COUNT, ACTIVE_ROOMS_LIST, AUDIO_STATUS, VIDEO_STATUS, DEFAULT_EMOTICON_ARRAY,
    CALL_STATUS, SET_CALL_STATUS, REACTIONS_ROOT, ACTIVITY_MAP, QUESTIONS, SELECTED_EMOJI_ARRAY, HOST_UID, CO_HOST_UID, EMOTICONS_STATUS
} from './utils/Constants';
import QuizComponent from './components/QuizComponent'
import ButtonComp from './components/ButtonComp'
import database from '@react-native-firebase/database';
// import firestore from '@react-native-firebase/firestore';
import { onClearData, onUpdateCount, onUpdateReactionMapper, setReactionMap } from './redux/action'
import { ChannelProfileType, ClientRoleType, createAgoraRtcEngine, RtcSurfaceView, VideoViewSetupMode } from "react-native-agora";
import { connect } from 'react-redux';
import ToolbarComp from './components/ToolbarComp';
import HostOptionsComp from './components/HostOptionsComp';
import UserInteractionBottomComp from './components/UserInteractionBottomComp';
import PollList from './components/PollList';
import ImageTextComp from './components/ImageTextComp';
import WinnersComp from './components/WinnersComp';
import AnimatedEmoticonsComp from './components/AnimatedEmoticonsComp';
import CallCountdownComp from './components/CallCountdownComp';
import Toast from 'react-native-simple-toast';
import { getAllQuestions, recordAnsForUser, getResultForQuestion, getLeaderboardForEvent } from './api/polls';
import { createEvent } from '../src/api/event'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

class LiveStreamComp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            msg: "",
            callJoined: false,
            channelId: "",
            chatlist: [],
            totalComments: 0,
            isChatToggleEnabled: true,
            isReactionsEnabled: false,
            reactionsArr: [],
            isChatEnable: true,
            startTime: 0,
            ansTime: 0,
            isQuesVisible: false,
            isWinnersListVisible: false,
            currentQuestion: null,
            currentSelectedQuesValue: -1,
            questionsList: [],
            currentRoomKey: '',
            isPoll: false,
            pollTimer: 0,

            isAudioEnabled: true,
            isVideoEnabled: true,
            coHostUid: 0,
            hostUid: 0,
            defaultReactionMaps: {},
            deltaReactions: {},
            currentTimeStamp: 0,

            timeLimit: props.initialTimeLimit * 60 * 1000,         //In millisec
            reAttempts: props.reAttempts,
            currentAttempt: 0,
            toShowTimer: false,
            winnersList: [],

            connection: null,
        }
        this.onConferenceTerminated = this.onConferenceTerminated.bind(this);
        this.onConferenceJoined = this.onConferenceJoined.bind(this);
        this.onConferenceWillJoin = this.onConferenceWillJoin.bind(this);
    }

    /**
     * Adding firebase listener's for chat and viewer's count
     *
     */
    addFirebaseListeners(channelId) {
        // Putting a listener for reading chatlist
        database()
            .ref(ITEM_SEPERATOR + channelId + CHATS)
            .once('value')
            .then(snapshot => {
                let chatlist = this.state.chatlist;
                let data = snapshot.val();
                if (data != null) {
                    data.key = snapshot.key;
                    console.log("Chatlist once : ", data)
                    chatlist.push(data);
                    this.setState({
                        chatlist
                    })
                }
            });
        this.dbReference = database().ref(ITEM_SEPERATOR + channelId + CHATS)
            .on('child_added', snapshot => {
                let chatlist = this.state.chatlist;
                let data = snapshot.val();
                data.key = snapshot.key;
                chatlist.push(data);
                this.setState({
                    chatlist
                })
            })

        this.commentsCountRef = database().ref(ITEM_SEPERATOR + channelId + CHATS_COUNT)
            .on('value', snapshot => {
                this.setState({
                    totalComments: snapshot.val() ? snapshot.val() : 0
                })
            })

        // Putting a listener for reading live viewer's count
        this.countReference = database().ref(ITEM_SEPERATOR + channelId + TOTAL_AUDIENCE)
            .on('value', snapshot => {
                this.props.onUpdateCount(snapshot.val())
            })

        // Listening to chat visiblity for Co-Host
        if (this.props.user == CO_HOST || this.props.user == ATTENDEE) {
            // Putting a listener for listening to co-host chat visibility Todo
            this.chatReference = database().ref(ITEM_SEPERATOR + channelId + CHAT_VISIBILITY)
                .on('value', snapshot => {
                    this.setState({
                        isChatEnable: snapshot.val()
                    })
                })
            this.listenToWinnersList(channelId);
        }
        this.listenToQuestions(channelId);

        database()
            .ref(ITEM_SEPERATOR + channelId + HOST_UID)
            .once('value')
            .then(snapshot => {
                if (snapshot.val()) {
                    this.setState({
                        hostUid: snapshot.val()
                    })
                }
            });

        this.coHostReference = database().ref(ITEM_SEPERATOR + channelId + CO_HOST_UID)
            .on('value', snapshot => {
                // if (snapshot.val()) {
                this.setState({
                    coHostUid: snapshot.val()
                })
                // }
            })
    }

    askQuestion(value) {
        if (this.state.currentSelectedQuesValue == -1 || this.state.currentSelectedQuesValue == value) {
            let aCurrentQues = this.state.questionsList[value]
            this.setState({
                currentQuestion: aCurrentQues,
                currentSelectedQuesValue: value
            })

            // Setting the current question in firebase
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS)
                .update({
                    currentQuestion: aCurrentQues
                })
        } else {
            Toast.show('Another question is active.');
        }
    }

    startPollTimer() {
        setTimeout(() => {
            if (this.state.pollTimer < 15) {
                this.setState({
                    pollTimer: this.state.pollTimer + 1
                })
                this.startPollTimer();
            } else {
                // Get Results of the current ques from the app
                console.log(" current question to get answer for ", this.state.currentQuestion)
                if (this.props.user == HOST)
                    this.getQuestionResult();
                this.setState({
                    pollTimer: 0,
                    currentSelectedQuesValue: -1
                })
            }
        }, 1000);
    }

    getQuestionResult() {
        let res = getResultForQuestion({
            postId: this.state.currentQuestion.id,
            postType: this.props.eventType,
            authToken: this.props.authToken
        })
            .then((res) => {
                console.log('Result for question result: ', res.data.status);
                let currentQuestion = { ...this.state.currentQuestion }
                currentQuestion.pollOptions = res.data.data.post.pollOptionResponseDetails
                currentQuestion.answered = true

                this.setState({
                    currentQuestion: currentQuestion
                })

                const newOptions = this.state.questionsList.map((question, index) => {
                    let copyQuestion = { ...question };

                    if (copyQuestion.id === currentQuestion.id) {
                        copyQuestion.pollOptions = currentQuestion.pollOptions
                        copyQuestion.answered = true
                    }
                    return copyQuestion;
                });
                this.setState({
                    currentQuestion: currentQuestion,
                    questionsList: newOptions
                })
                database()
                    .ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS)
                    .update({
                        currentQuestion: currentQuestion
                    })

                return res.data
            })
            .catch((err) => {
                console.log('Error response of posting a question: ', err);
                return err
            })
    }

    startCallTimer() {
        const aTotalTime = this.state.timeLimit - (5 * 60 * 1000)
        clearTimeout(this.callEndAlertListener);
        this.callEndAlertListener = setTimeout(() => {
            this.showCallEndAlert();
            this.showCallEndCountdown();
        }, aTotalTime);
    }

    onJoinChannelSuccess = (connection) => {
        if (this.props.user == HOST) {
            database().ref(ITEM_SEPERATOR + connection.channelId + SET_CALL_STATUS)
                .update({
                    hostUid: connection.localUid,
                });
            this.setState({
                hostUid: connection.localUid,
                connection: connection,
            })
        } else if (this.props.user == CO_HOST) {
            database().ref(ITEM_SEPERATOR + connection.channelId + SET_CALL_STATUS)
                .update({
                    coHostUid: connection.localUid
                });
            this.setState({
                coHostUid: connection.localUid,
                connection: connection,
            })
        } else {
            this.setState({ connection: connection });
        }
        this.onConferenceJoined();
    }

    onError = (errorCode) => {
        if (errorCode == UNAUTHORIZED) {
            this.inValidToken()
        }
    }

    initAgora = async (chanellId, agoraAppId) => {
        this.AgoraEngineRef = createAgoraRtcEngine();
        this.AgoraEngineRef.initialize({
            appId: agoraAppId,
            channelProfile: ChannelProfileType.ChannelProfileLiveBroadcasting,
        });

        if (Platform.OS === 'android') {
            // Need granted the microphone and camera permission
            await PermissionsAndroid.requestMultiple([
              'android.permission.RECORD_AUDIO',
              'android.permission.CAMERA',
            ]);
          }

        await this.AgoraEngineRef?.enableLocalAudio(true);
        await this.AgoraEngineRef?.enableLocalVideo(true);

        await this.AgoraEngineRef.enableVideo();
        await this.AgoraEngineRef.startPreview()

        if (this.props.user == HOST || this.props.user == CO_HOST) {
            this.AgoraEngineRef.setClientRole(ClientRoleType.ClientRoleBroadcaster);
        }

        this.AgoraEngineRef.registerEventHandler({
            onJoinChannelSuccess: this.onJoinChannelSuccess,
            onError: this.onError
        })
    };

    showCallEndAlert() {
        const showAlertTime = Math.floor(Date.now() / 1000)
        const msg = this.state.currentAttempt < this.state.reAttempts ?
            "You stream will end in 5 mins, you want to extend by 30 mins?" : "You stream will end in 5 mins"
        Alert.alert(
            "Alert!",
            msg,
            [
                {
                    text: Strings.OK,
                    onPress: () => {
                        if (this.state.currentAttempt < this.state.reAttempts) {
                            const closeAlertTime = Math.floor(Date.now() / 1000)
                            const extraTimeinSec = 300 - (closeAlertTime - showAlertTime)
                            this.setState({
                                timeLimit: extraTimeinSec * 1000 + (30 * 60 * 1000),
                                currentAttempt: this.state.currentAttempt + 1
                            }, () => {
                                this.startCallTimer();
                                this.hideCallEndCountdown();
                            })
                        }
                    }
                },
                {
                    text: Strings.CANCEL,
                    onPress: () => { }
                }
            ],
            { cancelable: false }
        );
    }

    showCallEndCountdown() {
        this.setState({
            toShowTimer: true
        })
    }

    hideCallEndCountdown() {
        this.setState({
            toShowTimer: false
        })
    }

    listenToQuestions(channelId) {
        this.questionsRef = database()
            .ref(ITEM_SEPERATOR + channelId + QUESTIONS + '/currentQuestion')
            .on('value', snapshot => {
                if (snapshot.val()) {
                    console.log(" current question from firebase", snapshot.val())
                    this.setState({
                        currentQuestion: snapshot.val(),
                        isQuesVisible: true,
                        startTime: this.state.pollTimer > 0 ? this.state.startTime : new Date()
                    }, () => {
                        if (this.state.currentQuestion.answered || this.state.pollTimer > 0) {

                        } else {
                            this.startPollTimer();
                            this.ansTimeout = setTimeout(() => {
                                this.setState({
                                    ansTime: 0
                                }, () => {
                                    // this.props.onOptionSelected(null, 0);
                                })
                            }, 15000);
                        }
                    })
                } else {
                    this.setState({
                        currentQuestion: null,
                        isQuesVisible: false
                    })
                }
            });
    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.endCall);
        const url = this.props.callUrl  // can also be only room name and will connect to jitsi meet servers
        const userInfo = {
            displayName: this.props.userName,
            email: this.props.email,
            avatar: this.props.userImageUrl
        };

        this.joinStreaming(url, userInfo);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.endCall);
        clearTimeout(this.recationListener);
        this.recationListener = null;
        clearTimeout(this.callEndAlertListener);
        this.callEndAlertListener = null;
        this.removeFirebaseListeners();
        this.updateFirebase(this.state.currentRoomKey);

        if (this.props.videoMode == PREMIUM && this.AgoraEngineRef !== undefined)
            this.AgoraEngineRef?.destroy?.();
    }

    /**
     * Method to end/leave the stream and send the callback to the parent
     * component
     *
     */
    endCall = () => {
        if (this.props.user == HOST) {
            this.showAlert(Strings.ALERT, Strings.END_LIVE_STREAM);
        } else if (this.props.user == ATTENDEE || this.props.user == CO_HOST) {
            this.showAlert(Strings.ALERT, Strings.ARE_YOU_SURE_YOU_WANT_TO_LEAVE);
        }
        return true
    };

    fetchPostQuestions = (channelId, authToken) => {
        console.log('Getting all questions :', authToken);
        let res = getAllQuestions({ postId: this.props.eventId, authToken: authToken })
            .then((res) => {
                console.log('Response of all questions: ', res.data.data.content);
                // Remove the second variable in set State
                this.setState({
                    questionsList: res.data.data.content,
                })
                console.log('Setting response in firebase: ', res.data.data.content);
                database()
                    .ref(ITEM_SEPERATOR + channelId + QUESTIONS)
                    .set({
                        questions: res.data.data.content
                    })
                return res.data
            })
            .catch((err) => {
                console.log('Error response of all questions: ', res);
                return err
            })
    }

    onSwitchCamera = () => {
        if (this.props.videoMode == PREMIUM) {
            this.AgoraEngineRef.switchCamera();
        } else {
        }
    }

    toggleFlash = () => {
        const isSupported = this.AgoraEngineRef.isCameraTorchSupported()
        isSupported.then(function (result) {
            // here you can use the result of promiseB
        });
        if (this.props.videoMode == PREMIUM) {
            this.AgoraEngineRef.setCameraTorchOn(true);
        } else {

        }
    }

    /**
     * Shows an alert dialog with proper messages for {@link HOST} as well as
     * {@link ATTENDEE}
     *
     * @param {*} title
     * @param {*} msg
     */
    showAlert(title, msg) {
        Alert.alert(
            title,
            msg,
            [
                {
                    text: Strings.OK,
                    onPress: () => {
                        if (this.props.user == HOST || this.props.user == CO_HOST) {
                            this.setState({
                                currentQuestion: null,
                                isQuesVisible: false,
                                isWinnersListVisible: false
                            })
                        } else if (this.props.user == ATTENDEE) {
                            this.setState({
                                currentQuestion: null,
                                ansTime: 0,
                                startTime: 0,
                                isQuesVisible: false,
                                isWinnersListVisible: false
                            })
                        }
                        this.props.onCallEnded();
                    }
                },
                {
                    text: Strings.CANCEL,
                    onPress: () => { }
                }
            ],
            { cancelable: false }
        );
    }

    getTotalVotes(pollOptions) {
        let totalVotes = 0;
        for (var i = 0; i < pollOptions.length; i++) {
            totalVotes = pollOptions[i].voteCount + totalVotes
        }
        return totalVotes;
    }

    /**
     * Gets the value of the reaction from Redux, used to calculate the delta
     * and update the current value in Redux to calculate the delta for next
     * round.
     *
     * @param {*} key     Key for the reaction
     * @param {*} value   Reaction count
     */
    getValue(key, value) {
        let currentValue = this.props.chats.totalReactions[key]
        let localObj = { ...this.props.chats.totalReactions }
        localObj[key] = value
        this.props.onUpdateReactionMapper(localObj)
        if (currentValue == NaN || currentValue == undefined) {
            currentValue = 0
        }
        return currentValue
    }

    /**
     * Method that displays alert when a user joins a stream that has ended
     * and navigates back
     *
     */
    goBack() {
        Alert.alert(
            Strings.ALERT,
            Strings.LIVE_STREAM_ENDED,
            [
                {
                    text: Strings.OK,
                    onPress: () => {
                        this.props.onClearData();
                        this.props.onCallEnded();
                    }
                }
            ],
            { cancelable: false }
        );
    }

    /**
     * Method that displays alert when a user joins a stream that has ended
     * and navigates back
     *
     */
    inValidToken() {
        Alert.alert(
            Strings.ALERT,
            Strings.UNAUTHORIZED,
            [
                {
                    text: Strings.OK,
                    onPress: () => {
                        this.props.onClearData();
                        this.props.onCallEnded();
                    }
                }
            ],
            { cancelable: false }
        );
    }

    /**
     * Join/Start the live stream depending on the user type
     * {@link ATTENDEE} will join an on going live stream or show an exit dialog
     * if the stream has ended.
     * {@link HOST} will start a new live stream
     *
     * @param {*} url        Url of the live stream
     * @param {*} userInfo   Info of the user including name, profile image url and email
     */
    joinStreaming(url, userInfo) {
        const { userName, password, callUrl, videoMode, user } = this.props
        var channelId = callUrl.replace(BASE_URL, "");
        console.log("url : ", callUrl)
        if (user == ATTENDEE) {
            database()
                .ref(ITEM_SEPERATOR + channelId + CALL_STATUS)
                .once('value')
                .then(snapshot => {
                    console.log("Call status of attendde : ", snapshot.val)
                    if (snapshot.val()) {
                        if (videoMode == PREMIUM) {
                            this.initAgora(channelId, this.props.agoraAppId).then(() => {
                                this.AgoraEngineRef.joinChannel(this.props.token, channelId, this.props.userId, {
                                    clientRoleType: ClientRoleType.ClientRoleBroadcaster,
                                })
                            });
                        } else if (videoMode == NORMAL) {

                        }
                    } else {
                        this.goBack();                              // Stream has ended
                    }
                });

            this.audioReference = database()
                .ref(ITEM_SEPERATOR + channelId + AUDIO_STATUS)
                .on('value', snapshot => {
                    if (snapshot.val() !== null || snapshot.val() !== undefined) {
                        this.setState({
                            isAudioEnabled: snapshot.val()
                        })
                    }
                });
            this.videoReference = database()
                .ref(ITEM_SEPERATOR + channelId + VIDEO_STATUS)
                .on('value', snapshot => {
                    if (snapshot.val() !== null || snapshot.val() !== undefined) {
                        this.setState({
                            isVideoEnabled: snapshot.val()
                        }, () => {
                        })
                    }
                });
        } else {
            if (this.props.eventId != null) {
                if (videoMode == PREMIUM) {
                    this.initAgora(channelId, this.props.agoraAppId).then(() => {
                        this.AgoraEngineRef.joinChannel(this.props.token, channelId, this.props.userId, {
                            clientRoleType: ClientRoleType.ClientRoleBroadcaster,
                        })
                    });
                } else if (videoMode == NORMAL) {

                }
            } else {
                const data = {
                    deliverByDate: '',
                    description: 'broadcast event',
                    eventEndDate: '',
                    eventStartDate: new Date(),
                    eventType: 'BROADCAST',
                    joinFee: 0,
                    partcipantFee: 0,
                    seatsAvailable: 0,
                    title: 'Broadcast event',
                };

                createEvent({ authToken: this.props.authToken, data }).then((createEventResult) => {
                    if (createEventResult.status) {
                        const eventId = createEventResult.data.id;
                        const streamUrl = BASE_URL + eventId;
                        this.setState({ channelId: eventId }, () => {
                            if (videoMode == PREMIUM) {
                                this.initAgora(channelId, this.props.agoraAppId).then(() => {
                                    this.AgoraEngineRef.joinChannel(this.props.token, channelId, null, this.props.userId)
                                });
                            } else if (videoMode == NORMAL) {

                            }
                        })
                    } else {
                        alert(createEventResult.data);
                        this.props.onCallEnded();
                    }
                }).catch(() => this.props.onCallEnded());
            }
        }
    }

    onConferenceTerminated(nativeEvent) {
        /* Conference terminated event */
    }

    onConferenceJoined() {
        var channelId = this.props.eventId != null ? this.props.eventId : this.state.channelId;
        this.setState({
            callJoined: true,
            channelId: channelId,
            isPoll: this.props.isPoll
        })
        // Creating db and setting call status for other users to join
        if (this.props.user == HOST) {
            this.startCallTimer();

            const { userId, guestId, isScheduledEvent, eventType } = this.props;
            // const newEvent = {
            //     starId: userId,
            //     startTime: firestore.FieldValue.serverTimestamp(),
            //     eventType,
            //     isScheduledEvent,
            //     guestId: guestId,
            // };

            const newReference = database().ref(ITEM_SEPERATOR + channelId + SET_CALL_STATUS);
            newReference.update({
                isActive: true,
                isVideoOn: this.state.isVideoEnabled,
                isAudioOn: this.state.isAudioEnabled
            })
                .then(() => {

                });

            // firestore()
            //     .collection('star_events')
            //     .doc(`${channelId}`)
            //     .set({ ...newEvent })
            //     .then((res) => { })
            //     .catch((err) => { });

            const callStatusReference = database().ref(ITEM_SEPERATOR + ACTIVE_ROOMS_LIST).push();
            this.setState({
                currentRoomKey: callStatusReference.key
            })
            callStatusReference.set({
                roomName: channelId
            })

            // database()
            //     .ref(ITEM_SEPERATOR + channelId + QUESTIONS)
            //     .set({
            //         questions: this.props.questionsList
            //     })
            this.fetchPostQuestions(channelId, this.props.authToken);

            this.updateChatVisibility(this.props.chatEnable, channelId)
            this.updateReactionsVisibility(this.props.reactionsArr && this.props.reactionsArr.length > 0 ? true : false, channelId)
            this.updateReactionsArray(this.props.reactionsArr && this.props.reactionsArr.length > 0 ? this.props.reactionsArr : [], channelId)
            this.setState({
                isChatToggleEnabled: this.props.chatEnable,
                isReactionsEnabled: this.props.reactionsArr && this.props.reactionsArr.length > 0 ? true : false,
                reactionsArr: this.props.reactionsArr && this.props.reactionsArr.length > 0 ? this.props.reactionsArr : []
            }, () => {
                if (this.state.isReactionsEnabled) {
                    this.setReactionMapper();
                }
            })
        } else if (this.props.user == ATTENDEE || this.props.user == CO_HOST) {
            /** Putting a listener on call status(Used when the host ends the live stream)
             *  and navigating properly after the call has ended.
             *
             */
            this.statusReference = database()
                .ref(ITEM_SEPERATOR + channelId + CALL_STATUS)
                .on('value', snapshot => {
                    if (!snapshot.val()) {
                        // Live Stream has ended
                        if (this.props.videoMode == PREMIUM) {
                            this.AgoraEngineRef.leaveChannel()
                        } else {
                        }
                        this.setState({
                            callJoined: false
                        })
                        this.goBack();
                    } else {
                        // Increment the total live audience counter
                        if (this.isUpdatedLiveCount === undefined) {
                            this.isUpdatedLiveCount = true;
                            this.updateLiveAudienceCount(true);
                        }
                    }
                });

            this.emoticonsReference = database()
                .ref(ITEM_SEPERATOR + channelId + SELECTED_EMOJI_ARRAY)
                .on('value', snapshot => {
                    let data = snapshot.val();
                    if (data && data.length > 0) {
                        let aTempObj = this.state.defaultReactionMaps
                        for (const element of data) {
                            aTempObj[element] = 0
                        }
                        this.setState({
                            defaultReactionMaps: aTempObj
                        })
                    }
                    this.setState({
                        reactionsArr: data && data.length > 0 ? data : []
                    })
                });
            this.emoticonsStatusReference = database()
                .ref(ITEM_SEPERATOR + channelId + EMOTICONS_STATUS)
                .on('value', snapshot => {
                    let data = snapshot.val();
                    if (data) {
                        this.setReactionMapper();
                    }
                    this.setState({
                        isReactionsEnabled: data ? data : false
                    })
                });
        }
        this.addFirebaseListeners(channelId);
    }

    onConferenceWillJoin(nativeEvent) {
        /* Conference will join event */
    }

    /**
    * Listener for text changes in msg text input
    *
    * @param {*} value
    */
    onMsgUpdated(value) {
        this.setState({
            msg: value
        })
    }

    onOptionSelected(value) {
        clearTimeout(this.ansTimeout);
        this.setState({
            ansTime: new Date() - this.state.startTime
        }, () => {
            this.selectOptionForPost(value, this.state.ansTime);
        })
    }

    selectOptionForPost(value, ansTime) {
        const selectedIdsArr = []
        selectedIdsArr.push(value.id)
        let res = recordAnsForUser({
            postId: this.state.currentQuestion.id,
            postType: this.props.eventType,
            authToken: this.props.authToken,
            selectdOptionsArray: selectedIdsArr,
            timeInSec: ansTime / 1000
        })
            .then((res) => {
                console.log('Question answered: ', res.data.status);
                return res.data
            })
            .catch((err) => {
                console.log('Error response of posting a question: ', err);
                return err
            })
    }

    onCloseQues() {
        this.setState({
            isQuesVisible: false
        })

        // Setting the current question to null in firebase if it's a HOST
        if (this.props.user === HOST) {
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS)
                .update({
                    currentQuestion: null
                })
        }
    }

    /**
     * Writes the data to firebase
     *
     */
    onReact(emoticon) {
        const newReference = database().ref(ITEM_SEPERATOR + this.state.channelId + REACTIONS_ROOT + ITEM_SEPERATOR + emoticon)
            .transaction(currentReactions => {
                if (currentReactions === null) return 1;
                return currentReactions + 1;
            })
            .then(transaction => { });
    }

    /**
     * Method to write message in firebase and clear the char input
     *
     */
    onSendMsg() {
        if (this.state.msg.trim().length > 0) {
            const newReference = database().ref(ITEM_SEPERATOR + this.state.channelId + CHATS).push();
            newReference.set({
                msg: this.state.msg,
                profileName: this.props.userName,
                imageUrl: this.props.userImageUrl
            })
                .then(() => {
                    this.setState({
                        msg: ""
                    })
                });
            const countReference = database().ref(ITEM_SEPERATOR + this.state.channelId + CHATS_COUNT)
                .transaction(totalComments => {
                    if (totalComments === null) return 1;
                    return totalComments + 1;
                })
                .then(transaction => { });
        }
    }

    /**
     * Removing the listeners
     *
     */
    removeFirebaseListeners() {
        try {
        // Removing listener for call status in case of ATTENDEE
        if (this.props.user == ATTENDEE) {
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS + '/winnersList')
                .off('value', this.winnersListRef);
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + AUDIO_STATUS)
                .off('value', this.audioReference);
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + VIDEO_STATUS)
                .off('value', this.videoReference);
        }
        // Removing listener for chats
        database().ref(ITEM_SEPERATOR + this.state.channelId + CHATS)
            .off('child_added', this.dbReference);
        // Removing listener for chat count
        database().ref(ITEM_SEPERATOR + this.state.channelId + CHATS_COUNT)
            .off('child_added', this.commentsCountRef);
        // Removing listener for the viewer's count
        database().ref(ITEM_SEPERATOR + this.state.channelId + TOTAL_AUDIENCE)
            .off('value', this.countReference);
        if (this.props.user == CO_HOST || this.props.user == ATTENDEE) {
            // Removing listener for the co-host chat visibility
            database().ref(ITEM_SEPERATOR + this.state.channelId + CHAT_VISIBILITY)
                .off('value', this.chatReference);

            database().ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS + '/currentQuestion')
                .off('value', this.currentQuestion);

            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + CALL_STATUS)
                .off('value', this.statusReference);

            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + SELECTED_EMOJI_ARRAY)
                .off('value', this.emoticonsReference);
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + EMOTICONS_STATUS)
                .off('value', this.emoticonsStatusReference);
        }
        database()
            .ref(ITEM_SEPERATOR + this.state.channelId + CO_HOST_UID)
            .off('value', this.coHostReference);
        } catch(err) {
            console.log({ err });
        }
    }

    /**
     * Method that returns the message item component for chat list
     *
     * @param {*} param0
     */
    _renderItemComponent = ({ item, index }) => {
        return (
            <MessageComp
                imageUrl={item.imageUrl}
                name={item.profileName}
                msg={item.msg}
            />
        )
    }

    _renderFanEmoticons = ({ item, index }) => {
        const emoji = EMOTICONS[item]
        return (
            <Pressable
                onPress={this.onReact.bind(this, item)}>
                <Text
                    style={Styles.emoticonStyle}>
                    {emoji}
                </Text>
            </Pressable>
        )
    }

    /**
     * Method that starts a listener that fetches the reaction count every second,
     * calculates the delta and updates in the Redux.
     *
     */
    setReactionMapper() {
        this.recationListener = setTimeout(() => {
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + REACTIONS_ROOT)
                .once('value')
                .then(snapshot => {
                    let data = snapshot.val();
                    let reactionArr = [...this.props.chats.reactions]
                    if (data) {
                        let deltaReactions = Object.fromEntries(
                            // convert to array, map, and then fromEntries gives back the object
                            Object.entries(data).map(([key, value]) =>
                                [key, value - this.getValue(key, value)]
                            )
                        );
                        reactionArr.push(deltaReactions);
                        this.setState({
                            deltaReactions: deltaReactions,
                            currentTimeStamp: new Date().getTime()
                        })
                    } else {
                        // reactionArr.push(this.state.defaultReactionMaps);
                    }

                    this.props.setReactionMap(reactionArr);
                    this.setReactionMapper();
                });
        }, 1500);
    }

    /**
     * Method that get's called when the chat visiility switch is toggled.
     *
     */
    toggleChat = () => {
        this.updateChatVisibility(!this.state.isChatToggleEnabled, this.state.channelId)
        this.setState({
            isChatToggleEnabled: !this.state.isChatToggleEnabled
        })
    }

    toggleReaction = () => {
        this.updateReactionsVisibility(!this.state.isReactionsEnabled, this.state.channelId)

        if (!this.state.isReactionsEnabled && this.state.reactionsArr.length == 0) {
            this.updateReactionsArray(DEFAULT_EMOTICON_ARRAY, this.state.channelId)
        }
        this.setState({
            reactionsArr: !this.state.isReactionsEnabled && this.state.reactionsArr.length == 0 ? DEFAULT_EMOTICON_ARRAY : this.state.reactionsArr,
            isReactionsEnabled: !this.state.isReactionsEnabled
        }, () => {
            if (this.state.isReactionsEnabled) {
                this.setReactionMapper();
            }
        })
    }

    toggleAudio = async () => {
        if (this.props.videoMode == PREMIUM) {
            await this.AgoraEngineRef?.enableLocalAudio(!this.state.isAudioEnabled);
        } else {
        }
        database().ref(ITEM_SEPERATOR + this.state.channelId + SET_CALL_STATUS)
            .update({
                isAudioOn: !this.state.isAudioEnabled
            });
        this.setState({
            isAudioEnabled: !this.state.isAudioEnabled
        })
    }

    toggleVideo = async () => {
        if (this.props.videoMode == PREMIUM) {
            await this.AgoraEngineRef?.enableLocalVideo(!this.state.isVideoEnabled);
        } else {
        }
        database().ref(ITEM_SEPERATOR + this.state.channelId + SET_CALL_STATUS)
            .update({
                isVideoOn: !this.state.isVideoEnabled
            });
        this.setState({
            isVideoEnabled: !this.state.isVideoEnabled
        })
    }

    /**
     * Updates the firebase with chat visibility
     *
     * @param {*} value
     */
    updateChatVisibility(value, channelId) {
        database().ref(ITEM_SEPERATOR + channelId + SET_CALL_STATUS)
            .update({
                chatVisible: value
            })
            .then(() => { });
    }

    /**
     * Updates the firebase with chat visibility
     *
     * @param {*} value
     */
    updateReactionsVisibility(value, channelId) {
        database().ref(ITEM_SEPERATOR + channelId + SET_CALL_STATUS)
            .update({
                reactionVisible: value
            })
            .then(() => { });
    }

    /**
    * Updates the firebase with emoticons
    *
    * @param {*} value
    */
    updateReactionsArray(value, channelId) {
        const reactionsNameArr = this.getReactionsArr(value)
        let aTempObj = this.state.defaultReactionMaps
        for (const element of reactionsNameArr) {
            aTempObj[element] = 0
        }
        this.setState({
            defaultReactionMaps: aTempObj
        })
        database().ref(ITEM_SEPERATOR + channelId + SET_CALL_STATUS)
            .update({
                reactionsArray: reactionsNameArr
            })
            .then(() => { });
    }

    getReactionsArr(value) {
        let arr = [];
        for (let i = 0; i < value.length; i++) {
            arr.push(Object.keys(EMOTICONS).find(key => EMOTICONS[key] === value[i]))
        }

        return arr;
    }

    /**
    * Updates the firebase that the {@link HOST} has ended the live stream or
    * the {@link ATTENDEE} has left the live stream
    *
    */
    updateFirebase(currentRoomKey) {
        if (this.props.videoMode == PREMIUM) {
            if (this.AgoraEngineRef !== undefined)
                this.AgoraEngineRef.leaveChannel()
        } else {
        }
        if (this.props.user == HOST) {
            const newReference = database().ref(ITEM_SEPERATOR + this.state.channelId + SET_CALL_STATUS);
            newReference.update({
                isActive: false,
                hostUid: 0,
                coHostUid: 0
            })
                .then(() => {
                    database()
                        .ref(ITEM_SEPERATOR + this.state.channelId + ACTIVITY_MAP)
                        .set({
                            reactions: this.props.chats.reactions
                        })
                        .then(() => {
                            this.props.onClearData();
                        })
                });
            // firestore().collection('star_events').doc(`${this.state.channelId}`).delete().catch();
            database().ref(ITEM_SEPERATOR + ACTIVE_ROOMS_LIST + ITEM_SEPERATOR + currentRoomKey)
                .set({
                    roomName: null
                })
        } else if (this.props.user == ATTENDEE) {
            if (this.state.channelId != null && this.state.channelId.toString().length > 0) {
                this.updateLiveAudienceCount(false);
                this.isUpdatedLiveCount = false;
            }
            this.props.onClearData();
        } else if (this.props.user == CO_HOST) {
            this.props.onClearData();
            this.updateLiveAudienceCount(false);
            this.isUpdatedLiveCount = false;
            const newReference = database().ref(ITEM_SEPERATOR + this.state.channelId + SET_CALL_STATUS);
            newReference.update({
                coHostUid: 0
            })
        }
    }

    /**
     * Updates the total live audience count in firebase db based on
     * the boolean passes using a Firebase Transaction
     *
     * @param {*} toIncrement wether the user has joined(True) or is leaving(False)
     */
    updateLiveAudienceCount(toIncrement) {
        const reference = database().ref(ITEM_SEPERATOR + this.state.channelId + TOTAL_AUDIENCE)
            .transaction(totalCount => {
                if (toIncrement) {
                    if (totalCount === null) return 1;
                    return totalCount + 1;
                } else {
                    if (totalCount === null) return 0;
                    return totalCount - 1;
                }
            })
            .then(transaction => { });
    }

    onCloseWinnersList() {
        this.setState({
            isWinnersListVisible: false,
            // winnersList: []
        })

        // Updating the winners list to null in firebase
        database()
            .ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS)
            .update({
                winnersList: null
            })
    }

    onShowResult() {
        if (this.state.winnersList.length > 0) {
            this.setState({
                isWinnersListVisible: true,
            })
            database()
                .ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS)
                .update({
                    winnersList: this.state.winnersList
                })
        } else {
            this.getFinalResults();
        }
    }

    getFinalResults() {
        let res = getLeaderboardForEvent({
            authToken: this.props.authToken,
            postId: this.props.eventId
        })
            .then((res) => {
                this.setState({
                    isWinnersListVisible: true,
                    winnersList: res.data.data
                })
                database()
                    .ref(ITEM_SEPERATOR + this.state.channelId + QUESTIONS)
                    .update({
                        winnersList: res.data.data
                    })
                return res.data
            })
            .catch((err) => {
                return err
            })
    }

    listenToWinnersList(channelId) {
        this.winnersListRef = database()
            .ref(ITEM_SEPERATOR + channelId + QUESTIONS + '/winnersList')
            .on('value', snapshot => {
                if (snapshot.val()) {
                    this.setState({
                        winnersList: snapshot.val,
                        isWinnersListVisible: true
                    })
                }
            });
    }

    getUserHelpers = () => {
        const { isAudioEnabled, isVideoEnabled } = this.state
        let userText = !isAudioEnabled ? "Host has muted self" : ""
        userText = !isVideoEnabled ? "Host has turned off the camera" : userText
        userText = !isAudioEnabled && !isVideoEnabled ? "Host has turned off the camera & muted self" : userText

        const imageRootStyle = !isAudioEnabled && !isVideoEnabled ? Styles.userRootSmallImageStyle : Styles.userRootLargeImageStyle
        const imageStyle = !isAudioEnabled && !isVideoEnabled ? Styles.userHelpSmallImageStyle : Styles.userHelpImageStyle


        return (
            <View
                style={Styles.userHelperRoot}>
                <View
                    style={Styles.horizontalRoot}>
                    {
                        !isVideoEnabled ?
                            <View
                                style={[Styles.userHelpImageRootStyle, Styles.imageRootStyle]}>
                                <Image
                                    style={imageStyle}
                                    source={require('../assets/images/video.png')}
                                />
                            </View>
                            : null}
                    {
                        !isAudioEnabled ?
                            <View
                                style={[Styles.userHelpImageRootStyle, Styles.imageRootStyle]}>
                                <Image
                                    style={imageStyle}
                                    source={require('../assets/images/audio.png')}
                                />
                            </View>
                            : null
                    }
                </View>
                <Text
                    style={Styles.userHelperTextStyle}>
                    {userText}
                </Text>
            </View>
        );
    }

    render() {
        const { user, videoMode, eventType } = this.props;
        const { callJoined, isChatToggleEnabled, isReactionsEnabled, isChatEnable, questionsList, totalComments, pollTimer, isAudioEnabled, isVideoEnabled, deltaReactions,
            currentQuestion, winnersList, hostUid, coHostUid, isQuesVisible, isWinnersListVisible, reactionsArr, chatlist, isPoll, currentTimeStamp, toShowTimer, connection } = this.state;

        let commentsText = isChatToggleEnabled ? totalComments + ' Comments' : 'Comments turned off';
        let leaveLabel = isPoll && user == HOST ? "End Poll" : Strings.LEAVE
        let otherUserUid = user == HOST ? coHostUid : (user == CO_HOST ? hostUid : 0)

        const tileStyle = coHostUid > 0 ? Styles.tilescreen : Styles.fullscreen
        return (
            <View style={{ backgroundColor: Colors.BLACK, flex: 1, width: '100%', height: '100%' }}>
                {videoMode == PREMIUM && connection
                    ?
                        <View
                            style={Styles.root}>
                            {user == HOST || user == CO_HOST ?
                                <View>
                                    {
                                        otherUserUid > 0 ?
                                            <RtcSurfaceView
                                                canvas={{ uid: otherUserUid, setupMode: VideoViewSetupMode.VideoViewSetupReplace }}
                                                style={tileStyle}
                                                />
                                            : null
                                    }
                                    <RtcSurfaceView
                                        canvas={{ uid: 0, setupMode: VideoViewSetupMode.VideoViewSetupReplace }}
                                        style={tileStyle}
                                        />
                                </View>
                                : <View>
                                    {isVideoEnabled ?
                                        <View>
                                            {
                                                hostUid > 0
                                                    ?
                                                        (
                                                            <RtcSurfaceView
                                                                canvas={{ uid: hostUid, setupMode: VideoViewSetupMode.VideoViewSetupReplace }}
                                                                style={tileStyle}
                                                            />
                                                        )
                                                    : null
                                            }
                                            {
                                                coHostUid > 0 ?
                                                    <RtcSurfaceView
                                                        canvas={{ uid: coHostUid, setupMode: VideoViewSetupMode.VideoViewSetupReplace }}
                                                        style={tileStyle}
                                                        />
                                                    : null
                                            }
                                        </View>
                                        : null
                                    }
                                </View>
                            }
                        </View>
                    :
                        <View style={Styles.root} />
                }
                {callJoined ?
                    <View style={Styles.toolbarRoot}>
                        {
                            (user == HOST || user == CO_HOST) && !isPoll ?
                                <ToolbarComp
                                    onCancelCall={this.endCall}
                                    onFlashOff={this.toggleFlash}
                                    onFlashOn={this.toggleFlash}
                                    onSwitchCamera={this.onSwitchCamera} />
                                : null
                        }

                        <LiveCounterComp
                            totalCount={this.props.chats.liveAudienceCount}
                            style={Styles.liveCounterStyle} />
                    </View>
                    : null
                }
                {
                    user == ATTENDEE && (!isAudioEnabled || !isVideoEnabled) ?
                        this.getUserHelpers()
                        : null
                }
                {callJoined && !isPoll ?
                    <View style={Styles.chatRoot}>
                        {
                            user == ATTENDEE && !isChatEnable ?
                                null :
                                <UserInteractionBottomComp
                                    chatList={chatlist}
                                    totalComments={totalComments}
                                    endCall={this.endCall}
                                    reactionArr={reactionsArr}
                                    deltaReactions={deltaReactions}
                                    onReaction={this.onReact.bind(this)}
                                    onReactionsEnabled={isReactionsEnabled}
                                    currentTimeStamp={currentTimeStamp}
                                    user={user} />
                        }

                        {
                            user == HOST ?
                                <HostOptionsComp
                                    style={Styles.hostOptionsRoot}
                                    onMessageEnabled={isChatToggleEnabled}
                                    onMessageToggle={this.toggleChat}
                                    onReactionEnabled={isReactionsEnabled}
                                    onReactionToggle={this.toggleReaction}
                                    reactionArr={reactionsArr}
                                    onCameraEnabled={isVideoEnabled}
                                    onCameraToggle={this.toggleVideo}
                                    onMikeEnabled={isAudioEnabled}
                                    onMikeToggle={this.toggleAudio} />
                                : null
                        }
                        {
                            user == HOST ?
                                <View
                                    style={Styles.bottomRoot}>
                                    {isChatToggleEnabled ?
                                        <Image
                                            style={Styles.tint}
                                            source={require('../assets/images/comment.png')} />
                                        : null
                                    }
                                    <Text
                                        style={Styles.commentsCountRoot}>
                                        {commentsText}
                                    </Text>
                                    <ButtonComp
                                        label={"End Livestream"}
                                        onPress={this.endCall} />
                                </View>
                                : null
                        }
                        {
                            user == ATTENDEE && isChatEnable ?
                                <KeyboardAvoidingView
                                    style={Styles.bottomRoot}>
                                    <TextInput
                                        style={Styles.msgRoot}
                                        placeholder={Strings.ENTER_MESSAGE}
                                        onChangeText={this.onMsgUpdated.bind(this)}
                                        value={this.state.msg}
                                        placeholderTextColor={Colors.THUMB_COLOR_DISABLED} />

                                    <ButtonComp
                                        buttonStyle={{ backgroundColor: '#FF3EA6' }}
                                        label={"Send"}
                                        onPress={this.onSendMsg.bind(this)} />
                                </KeyboardAvoidingView>
                                : null
                        }

                    </View>
                    : null
                }
                {
                    (user == ATTENDEE && callJoined) || isPoll ?
                        <ButtonComp
                            buttonStyle={[{ backgroundColor: '#FF3EA6' }]}
                            style={Styles.leaveBtnStyle}
                            label={leaveLabel}
                            onPress={this.endCall} />
                        : null
                }
                {
                    isPoll && reactionsArr.length > 0 ?
                        <View
                            style={Styles.chatRoot}>
                            <AnimatedEmoticonsComp
                                deltaReactions={deltaReactions}
                                timeStamp={currentTimeStamp} />
                        </View>
                        : null
                }
                {
                    user == HOST && callJoined && isPoll ?
                        <PollList
                            quesList={questionsList}
                            eventType={eventType}
                            askQues={this.askQuestion.bind(this)}
                            showResult={this.onShowResult.bind(this)} />
                        : null
                }

                {
                    isPoll && isQuesVisible ?
                        <View
                            style={Styles.pollTimerRoot}>
                            {!currentQuestion.answered && pollTimer > 0 ?
                                <ImageTextComp
                                    value={pollTimer + ' sec'}
                                    imgSrc={'../assets/images/close.png'} />
                                : null
                            }
                            {
                                currentQuestion && currentQuestion.answered ?
                                    <ImageTextComp
                                        value={this.getTotalVotes(currentQuestion.pollOptions)}
                                        imgSrc={'../assets/images/close.png'} />
                                    : null}

                        </View>
                        : null
                }
                {
                    isPoll ?
                        <QuizComponent
                            isVisible={isQuesVisible}
                            question={currentQuestion}
                            user={user}
                            eventType={eventType}
                            optionSelected={this.onOptionSelected.bind(this)}
                            onCloseQues={this.onCloseQues.bind(this)} />
                        : null
                }
                {
                    isPoll && winnersList.length > 0 ?
                        <WinnersComp
                            isVisible={isWinnersListVisible}
                            onCloseWinnersList={this.onCloseWinnersList.bind(this)}
                            winnersList={winnersList} />
                        : null
                }
                {
                    isPoll && user == ATTENDEE && reactionsArr.length > 0 ?
                        <FlatList
                            data={reactionsArr}
                            extraData={this.state}
                            renderItem={this._renderFanEmoticons}
                            style={Styles.emojiRootStyle}
                            horizontal={true}
                            contentContainerStyle={Styles.emojiContentStyle} />
                        : null
                }
                {
                    toShowTimer ?
                        <CallCountdownComp
                            timeUp={() => {
                                this.setState({
                                    currentQuestion: null,
                                    isQuesVisible: false,
                                    isWinnersListVisible: false
                                })
                                this.props.onCallEnded();
                            }} />
                        : null
                }
            </View>
        );
    }
}

const Styles = StyleSheet.create({
    root: {
        flex: 1,
        color: Colors.APP_DARK_BLUE,
    },
    toolbarRoot: {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0
    },
    bottomRoot: {
        zIndex: 9999,                              // Just to handle click on top of comments modal
        flexDirection: 'row',
        alignItems: 'center',
        height: Dimens.dimen_56,
        paddingHorizontal: Dimens.dimen_16,
        backgroundColor: Colors.DARK_BACKGROUND,
    },
    chatRoot: {
        height: windowHeight * 0.6,
        width: windowWidth,
        position: 'absolute',
        left: Dimens.dimen_0,
        bottom: Dimens.dimen_0,
        justifyContent: 'flex-end',
    },
    msgRoot: {
        paddingHorizontal: Dimens.dimen_16,
        paddingVertical: Dimens.dimen_4,
        marginEnd: Dimens.dimen_4,
        borderRadius: Dimens.dimen_8,
        borderWidth: Dimens.dimen_2,
        flex: 1,
        borderColor: Colors.WHITE,
        color: Colors.THUMB_COLOR_DISABLED,
        backgroundColor: Colors.WHITE,
        fontFamily: "SFUIText-Regular",
        fontWeight: "400"
    },
    commentsCountRoot: {
        paddingHorizontal: Dimens.dimen_16,
        paddingVertical: Dimens.dimen_4,
        color: Colors.WHITE,
        fontSize: Dimens.font_14,
        flex: 1,
        fontFamily: "SFUIText-Regular",
        fontWeight: "500"
    },
    horizontalRoot: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    userHelperRoot: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        bottom: 0,
        alignSelf: 'baseline',
        start: 0,
        end: 0
    },
    userRootLargeImageStyle: {
        width: Dimens.dimen_124,
        height: Dimens.dimen_124,
    },
    userRootSmallImageStyle: {
        width: Dimens.dimen_96,
        height: Dimens.dimen_96,
    },
    userHelpImageRootStyle: {
        backgroundColor: Colors.APP_DARK_BLUE_TRANSPARENT,
        borderWidth: Dimens.dimen_10,
        borderRadius: Dimens.dimen_64,
        borderColor: Colors.WHITE_TRANSPARENT,
        padding: Dimens.dimen_24,
        margin: Dimens.dimen_8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    userHelpImageStyle: {
        width: Dimens.dimen_64,
        height: Dimens.dimen_64,
        tintColor: Colors.WHITE
    },
    userHelpSmallImageStyle: {
        width: Dimens.dimen_56,
        height: Dimens.dimen_56,
        tintColor: Colors.WHITE
    },
    userHelperTextStyle: {
        fontSize: Dimens.font_16,
        color: Colors.WHITE,
        fontFamily: "SFUIText-Regular",
        fontWeight: "500"
    },
    buttonStyle: {
        marginHorizontal: Dimens.dimen_4
    },
    leaveBtnStyle: {
        backgroundColor: '#FF3EA6',
        borderRadius: Dimens.dimen_4,
        position: 'absolute',
        right: Dimens.dimen_0,
        top: Dimens.dimen_0,
        margin: Dimens.dimen_24,
        paddingHorizontal: Dimens.dimen_12,
    },
    liveCounterStyle: {
        margin: Dimens.dimen_16,
    },
    tilescreen: {
        width: windowWidth - 8,
        height: windowHeight / 2 - 4,
        backgroundColor: Colors.WHITE,
        borderRadius: Dimens.dimen_8,
        marginHorizontal: Dimens.dimen_4,
        marginVertical: Dimens.dimen_2
    },
    fullscreen: {
        width: windowWidth,
        height: windowHeight,
        // backgroundColor: Colors.WHITE
    },
    hostOptionsRoot: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0
    },
    pollTimerRoot: {
        position: 'absolute',
        right: 0,
        top: 100
    },
    emoticonStyle: {
        fontSize: Dimens.font_20,
        marginHorizontal: Dimens.dimen_12,
        margin: Dimens.dimen_8,
        padding: Dimens.dimen_4,
        textAlign: 'center',
        alignSelf: 'baseline'
    },
    emojiRootStyle: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: Colors.APP_DARK_BLUE,
        height: Dimens.dimen_56
    },
    emojiContentStyle: {
        flex: 1,
        alignItems: 'stretch',
        justifyContent: 'space-around'
    },
    tint: {
        tintColor: Colors.WHITE
    }
})

const mapStateToProps = (state) => {
    const { chats } = state
    return { chats }
};

export default connect(mapStateToProps, { onUpdateCount, onClearData, onUpdateReactionMapper, setReactionMap })(LiveStreamComp)
